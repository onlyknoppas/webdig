

# dig webapp

# Usage

## Development

fontend + api: `docker compose up dev_frontend_api`

api: `docker compose up dev_api`

frontend: `docker compose up dev_frontend`

Open `http://localhost:5123`

## Production
`docker build -t prod api` and `docker build -t prod frontend`

# Bugs

- [x] ANY query responds with 500 and loads very long
  - found out the reason, the dns library does not support that query
  - will implement it by extending the type query parameter to a list and using goroutines to make parallel dns queries
  - FIXED! 

# Fred log

## Roadmap
- [x] minimal Golang program that executes a DNS question on a nameserver
- [x] wrap into a gin rest api
- [x] frontend ui skeleton
- [x] react-query hook that requests api
- [x] api Dockerfile (dev & prod)
- [x] frontend Dockerfile (dev & prod)
- [x] compose file
- [x] gitlab CI api
- [x] gitlab CI frontend
- [x] backend unit tests
- [x] debug backend unit tests
- [ ] deploy to heroku(or something else) and netlify
- [ ] backend end2end tests
- [ ] frontend tests
- [x] ask nameservers in parallel with gorouting and respond with the fasted answer
- [ ] mount src and node_modules for fast hot-reloading in docker container during development
- [ ] create an api schema (for example OAS) that ideally can be consumed by the fronend for code generation
- [ ] parse record type specific display in frontend

I think it is of interest how I get to the solution so I will write a little log while I am developing.

## Thoughts on the general setup (2022-10-13 10:00 pm)
First of all, I want to reason about the general setup.

Frontend and api are in the same repo.

Everything can be started with one docker compose up

There will be one CI pipeline that tests and build api and frontend together.
Either package should only build if files in that package actually changed in the commit.

This is not necessary for this task as there is no significant code reuse between the frontend and the backend but I still like it that way.

### Backend api
I will build a golang RESTful API that will serve a single endpoint

GET /?domain=<DOMAIN>&recordtype=<RECORD_TYPE>

I will use the gin library.

To query the nameservers I will use https://github.com/miekg/dns

I plan to unittest everything for that https://github.com/foxcpp/go-mockdns looks very useful.

One could also end-to-end test the endpoint but its logic will be dead simple so I dont know if the effort is sensible.

### Frontend app
I will use the modern build system vite which, as the name suggests, is fast.

Much better developer experience than webpack and will save valuable CI pipeline resources.

To build the frontend I want to use antd design because I really like it and I used it a lot already.
I am thinking about actually building a second frontend also with MaterialUI because than it will look almost the same as the google app.


## First working version (2022-10-14 11:58 am)
I publish the first working version of the dig app.

The invested development time at this point is ~6h

I unfortunately did not measure the time exactly but it splits up into roughtly the following chunks:
 - ~0.5h reading
 - ~2h api implementation
 - ~1.5h frontend implementation
 - ~1.5h tooling setup
 - ~0.5h debugging

Following issues arose:

  1. I forgot that localhost is not accessible from outside the docker container, need to host with 0.0.0.0

  2. The frontend build failed because there was a typo in the Dockerfile ~~COYP . /.~~ but correct is: ./

  3. Monorepo caused complications with my vscode development setup. I had to add a go.work and open the frontend separately to get the linting to work

Unresolved issues:

  1. The ANY type query crashes the server

## Refactoring for testing (2022-10-16 01:14 pm)

I want to write unittests for the core functionality that exchanges dns messages to though the dns.Client.
For that I need to mock the dns.Client and inject it during testing.

A dnsClientFactory will be passed down to the lookup function that I exchange for the mock during testing.

Because I currently only use the Exchange function of the DNS client I change type of the factory output to an interface with just that function so that I do not have to mock the whole dns.Client interface.

Time used: ~2h

Next step: implementing the unit tests

## Unittests (2022-10-17 11:03 am)

Needed some more refactoring of the code so that mocking of relevant interfaces was easy.

The goal was to test each in functionality in the server pipeline seperately, the endpoint controller, the parallel lookup on multiple nameservers and the parsing of lookup responses.

I implemented the tests but they will require more time to debug them properly which I defer to some time later.

Also implemented a simple CI pipeline that runs go checks, the unittests and finally builds into an artifact that can be consumed by a deploy stage that does not exists yet.

For the tests two new dependencies are needed `github.com/stretchr/testify/{assert,mock}`

Time used: ~5.5h

Notable issues, challenges
  - reading and understanding the dns package types and interfaces so that I can mock the dns.Exchange function properly for testing

## CI Pipeline (2022-10-17 12:15 pm)
Fix the linter of the frontend package.
Had to comment out a lot of features that I actually liked to use to make it work.

Pipeline for the frontend. Jobs for api and frontend only run if the respective sources where changed.

Time used: ~1h

## Query multiple nameservers UI (2022-10-17 13:15 pm)
I read the task again and I realized that it should also display the results of queries to multiple nameservers. At first I just thought that backend api simply as a list of nameservers to check for redundancy.
Therefore added a simple UI build a list of nameservers to query.
Also added a new endpoint in the api that accepts an additional query parameter nameservers.
Ideally this endpoitn would be merge with the existing one, but to be faster with the implementation I just copy-pasted it.

The UI is working, but the backend query is not as it alwys returns 500

~0.45 hours

## Debugged backend unittests (2022-10-17 14:58 pm)
Problem: The dns.Msg object are assigned a random Id during the dns.Exchange.
This cannot be mocked. Had to work around it. It is a limitation of the test library that I used because it matches function arguments to mock function by argument equality and does not allow using a predicate function that could potentially ignore the different values of Msg.Id.
The workaround can only be applied to the specific test where I can identify the different Exchange calls by the nameserver used and not the Msg struct.
For a propert solution an actual implementation of a mock exchange function is required.

~1.0 hours

## Fixing the CI (2022-10-17 15:30 pm)
Api pipeline runs without failure.
Fixed frontend_install job by replacing node:18-alpine3.15 with node.

~ 30 minutes

# Resume

Of the tasklist that I set myself the following tasks are still open:

- [ ] mount src and node_modules for fast hot-reloading in docker container during development
- [ ] create an api schema (for example OAS) that ideally can be consumed by the fronend for code generation
- [ ] parse record type specific display in frontend
- [ ] deploy to heroku(or something else) and netlify
- [ ] backend end2end tests
- [ ] frontend tests

Also many of the tasks that I ticked were only fullfilled demonstratively and not exhaustively. For example, my test-coverage is very low.

There are new open tasks that arose with the addition of the GET /lookupNameservers endpoint that I could list here.

I expect that I would need around ~12 hours more to worktime to make this really perfect, robust and feature complete.

## Notes
Since this is a application task for a position as SRE I thought it is more important to focus on things that are actually related to SRE and not that much on features. Things like a CI and dev prod builds are actually not needed but I believe that these are relevant skills in my future work environment.
