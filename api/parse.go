package main

import (
	"strings"

	"github.com/miekg/dns"
)

func parseMXData(data string) string {
	parts := strings.SplitN(data, "\t", 5)
	if len(parts) >= 5 {
		return parts[4]
	}
	return ""
}

func parseHdr(h *dns.RR_Header) hdr {
	typeStr, ok := dns.TypeToString[h.Rrtype]
	if !ok || typeStr == "None" {
		typeStr = ""
	}
	return hdr{
		h.Name,
		typeStr,
		int64(h.Class),
		int64(h.Ttl),
		int64(h.Rdlength),
	}
}

func parseMsg(m *dns.Msg) *lookupResponse {
	response := new(lookupResponse)
	response.ID = int64(m.Id)
	response.Response = m.Response
	response.Opcode = int64(m.Opcode)
	response.Authoritative = m.Authoritative
	response.Truncated = m.Truncated
	response.RecursionAvailable = m.RecursionAvailable
	response.RecursionDesired = m.RecursionDesired
	response.Zero = m.Zero
	response.AuthenticatedData = m.AuthenticatedData
	response.CheckingDisabled = m.CheckingDisabled
	response.Rcode = int64(m.Rcode)
	response.Question = make([]question, len(m.Question))
	response.Answer = make([]answer, len(m.Answer))
	response.NS = make([]nS, len(m.Ns))
	response.Extra = make([]extra, len(m.Extra))

	for i, q := range m.Question {
		typeStr, ok := dns.TypeToString[q.Qtype]
		if !ok || typeStr == "None" {
			typeStr = ""
		}
		response.Question[i] = question{
			Name:   q.Name,
			Qtype:  typeStr,
			Qclass: int64(q.Qclass),
		}
	}

	for i, a := range m.Answer {
		response.Answer[i] = answer{
			Hdr:  parseHdr(a.Header()),
			MX:   a.String(),
			Data: parseMXData(a.String()),
		}
	}
	for i, a := range m.Ns {
		response.NS[i] = nS{
			Hdr:  parseHdr(a.Header()),
			MX:   a.String(),
			Data: parseMXData(a.String()),
		}
	}
	for i, a := range m.Extra {
		response.Extra[i] = extra{
			Hdr:  parseHdr(a.Header()),
			MX:   a.String(),
			Data: parseMXData(a.String()),
		}
	}
	return response
}
