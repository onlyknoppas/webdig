package main

import (
	"reflect"
	"testing"

	"github.com/miekg/dns"
	"github.com/stretchr/testify/assert"
)

func Test_parseMXData(t *testing.T) {
	data := [][2]string{
		{
			"konfidal.eu.\t86399\tIN\tA\t75.2.60.5",
			"75.2.60.5",
		},
		{
			"konfidal.eu.\t3582\tIN\tSOA\thydrogen.ns.hetzner.com. dns.hetzner.com. 2022091907 86400 10800 3600000 3600",
			"hydrogen.ns.hetzner.com. dns.hetzner.com. 2022091907 86400 10800 3600000 3600",
		},
		{
			"konfidal.eu.\t3599\tIN\tMX\t5 alt1.aspmx.l.google.com.",
			"5 alt1.aspmx.l.google.com.",
		},
		{
			"konfidal.eu.\t86399\tIN\tTXT\t\"google-site-verification=SJeDbw_ejPuBjKXnxwrWgY-8RhWq71y_TPpenZf1RjI\"",
			"\"google-site-verification=SJeDbw_ejPuBjKXnxwrWgY-8RhWq71y_TPpenZf1RjI\"",
		},
		{
			"konfidal.eu.\t86399\tIN\tTXT",
			"",
		},
	}
	for _, d := range data {
		assert.Equal(t, d[1], parseMXData(d[0]))
	}
}

func Test_parseHdr(t *testing.T) {
	data := []struct {
		*dns.RR_Header
		hdr
	}{
		{
			&dns.RR_Header{
				Name:     "tesla.com.",
				Rrtype:   dns.TypeA,
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
			hdr{
				Name:     "tesla.com.",
				Rrtype:   "A",
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
		},
		{
			&dns.RR_Header{
				Name:     "tesla.com.",
				Rrtype:   dns.TypeMX,
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
			hdr{
				Name:     "tesla.com.",
				Rrtype:   "MX",
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
		},
		{
			&dns.RR_Header{
				Name:     "tesla.com.",
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
			hdr{
				Name:     "tesla.com.",
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
		},
	}
	for _, d := range data {
		assert.Equal(t, d.hdr.Name, parseHdr(d.RR_Header).Name)
		assert.Equal(t, d.hdr.Rrtype, parseHdr(d.RR_Header).Rrtype)
		assert.Equal(t, d.hdr.Class, parseHdr(d.RR_Header).Class)
		assert.Equal(t, d.hdr.Ttl, parseHdr(d.RR_Header).Ttl)
		assert.Equal(t, d.hdr.Rdlength, parseHdr(d.RR_Header).Rdlength)
	}
}

func Test_parseMsg(t *testing.T) {
	mockAnswers := []mockRR{
		{
			H: dns.RR_Header{
				Name:     "tesla.com",
				Rrtype:   dns.TypeA,
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
			Data: "1.2.3.4",
		},
	}
	mockNs := []mockRR{
		{
			H: dns.RR_Header{
				Name:     "tesla.com",
				Rrtype:   dns.TypeNS,
				Class:    1,
				Ttl:      3600,
				Rdlength: 20,
			},
			Data: "subdomain.domain.com",
		},
	}
	msg := &dns.Msg{
		MsgHdr: dns.MsgHdr{
			Id:                 8,
			Response:           true,
			Opcode:             19,
			Authoritative:      true,
			RecursionDesired:   true,
			RecursionAvailable: true,
			Zero:               true,
			AuthenticatedData:  true,
			CheckingDisabled:   true,
			Rcode:              1,
		},
		Question: []dns.Question{
			{Name: "tesla.com",
				Qtype:  dns.TypeA,
				Qclass: 1},
		},
		Answer: []dns.RR{&mockAnswers[0]},
		Ns:     []dns.RR{&mockNs[0]},
		Extra:  []dns.RR{},
	}
	parsed := &lookupResponse{
		ID:                 8,
		Response:           true,
		Opcode:             19,
		Authoritative:      true,
		RecursionDesired:   true,
		RecursionAvailable: true,
		Zero:               true,
		AuthenticatedData:  true,
		CheckingDisabled:   true,
		Rcode:              1,
		Question: []question{{
			Name:   "tesla.com",
			Qtype:  "A",
			Qclass: 1,
		}},
		Answer: []answer{{
			Hdr: hdr{
				Name:     mockAnswers[0].Header().Name,
				Rrtype:   "A",
				Class:    1,
				Ttl:      3600,
				Rdlength: 4,
			},
			MX:   mockAnswers[0].String(),
			Data: mockAnswers[0].Data,
		}},
		NS: []nS{{
			Hdr: hdr{
				Name:     mockNs[0].Header().Name,
				Rrtype:   "NS",
				Class:    1,
				Ttl:      3600,
				Rdlength: 20,
			},
			MX:   mockNs[0].String(),
			Data: mockNs[0].Data,
		}},
		Extra: []extra{},
	}
	assert.True(t, reflect.DeepEqual(parsed, parseMsg(msg)))
}
