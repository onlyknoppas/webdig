package main

import (
	"strconv"
	"time"

	"github.com/miekg/dns"
	"github.com/stretchr/testify/mock"
)

type mockRR struct {
	dns.RR
	H    dns.RR_Header
	Data string
}

func (h *mockRR) Header() *dns.RR_Header { return h.header() }

func (h *mockRR) header() *dns.RR_Header { return &h.H }

func (h *mockRR) copy() dns.RR {
	panic("copy should never be called on mockRR")
}

func (h *mockRR) String() string {
	var s string

	if h.H.Rrtype == dns.TypeOPT {
		s = ";"
		// and maybe other things
	}

	s += h.H.Name + "\t"
	s += strconv.FormatInt(int64(h.H.Ttl), 10) + "\t"
	s += dns.Class(h.H.Class).String() + "\t"
	s += dns.TypeToString[h.H.Rrtype] + "\t"
	s += h.Data
	return s
}

func (h *mockRR) len(off int, compression map[string]struct{}) int {
	// l := dns.domainnamelen(h.header.Name, off, compression, true)
	// mock domainnamelen with constant value
	l := 7
	l += 10 // rrtype(2) + class(2) + ttl(4) + rdlength(2)
	return l
}

// Cannot simply mock these methods of RR because of the private types  used

// func (h *mockRR) pack(msg []byte, off int, compression compressionmap, compress bool) (off1 int, err error) {
// 	panic("pack should never be called on mockRR")
// }

// func (h *mockRR) unpack(msg []byte, off int) (int, error) {
// 	panic("unpack should never be called on mockRR")
// }

// func (h *mockRR) parse(c *zlexer, origin string) *parseerror {
// 	panic("parse should never be called on mockRR")
// }

func (h *mockRR) isDuplicate() bool {
	return false
}

func newMockMsg(q []*dns.Question, a []*mockRR, n []*mockRR, e []*mockRR) *dns.Msg {
	msg := &dns.Msg{
		MsgHdr: dns.MsgHdr{
			Id:                 8,
			RecursionDesired:   true,
			RecursionAvailable: true,
		},
	}
	if len(q) > 0 {
		msg.Question = make([]dns.Question, len(q))
	}
	if len(a) > 0 {
		msg.Answer = make([]dns.RR, len(a))
	}
	if len(n) > 0 {
		msg.Ns = make([]dns.RR, len(n))
	}
	if len(e) > 0 {
		msg.Extra = make([]dns.RR, len(e))
	}
	for i, qq := range q {
		msg.Question[i] = *qq
	}
	for i, aa := range a {
		msg.Answer[i] = aa
	}
	for i, nn := range n {
		msg.Ns[i] = nn
	}
	for i, ee := range e {
		msg.Extra[i] = ee
	}
	return msg
}

func newMockMsgNameError(q []*dns.Question) *dns.Msg {
	m := newMockMsg(q, nil, nil, nil)
	m.Rcode = dns.RcodeNameError
	return m
}

type mockLookupClient struct {
	mock.Mock
}

func (c *mockLookupClient) Exchange(m *dns.Msg, ns string) (*dns.Msg, time.Duration, error) {
	args := c.Called(m, ns)
	var retErr error
	if ret, ok := args[2].(error); ok {
		retErr = ret
	}
	var retDuration time.Duration
	if ret, ok := args[2].(time.Duration); ok {
		retDuration = ret
	}
	return args[0].(*dns.Msg), retDuration, retErr
}

func (c *mockLookupClient) Nameservers() []string {
	args := c.Called()
	return args[0].([]string)
}
