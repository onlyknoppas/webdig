package main

import "github.com/gin-gonic/gin"

type errBadRequest string

func sendError(c *gin.Context, status int, message string) {
	c.JSON(status, map[string]interface{}{"message": message})
}

func sendBadRequest(c *gin.Context, message string) {
	sendError(c, 400, message)
}

func sendInternal(c *gin.Context, message string, err error) {
	if err != nil {
		message = message + "; " + err.Error()
	}
	sendError(c, 500, message)
}

func sendNotFound(c *gin.Context, message string) {
	sendError(c, 404, message)
}
