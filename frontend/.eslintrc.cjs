module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    '@typescript-eslint/eslint-plugin',
    // 'react',
    // 'react-hooks',
    'prettier'
  ],
  extends: [
    // 'react-app',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    // 'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  root: true,
  ignorePatterns: ['**/*.eslintrc.cjs'],
  rules: {
    // '@typescript-eslint/interface-name-prefix': 'off',
    // '@typescript-eslint/explicit-function-return-type': 'off',
    // '@typescript-eslint/explicit-module-boundary-types': 'off',
    // '@typescript-eslint/no-explicit-any': 'off',
    // '@typescript-eslint/no-empty-fuction': 'off',
    // '@typescript-eslint/no-unused-vars': 'off',
  //   'react/react-in-jsx-scope': 'off',
    'indent': 'off',
  //   'react-hooks/exhaustive-deps': 'off',
    'prettier/prettier': ['error',
      {
        "singleQuote": true,
        "trailingComma": "all",
        "useTabs": true,
        "printWidth": 90,
      }
      , {}],
  },
};