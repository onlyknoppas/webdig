package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/go-playground/assert/v2"
	"github.com/miekg/dns"
	"github.com/stretchr/testify/mock"
)

// type mockLookupClient struct {
// 	mock.Mock
// }

// func (c *mockLookupClient) Exchange(m *dns.Msg, ns string) (*dns.Msg, time.Duration, error) {
// 	args := c.Called(m, ns)
// 	return args[0].(*dns.Msg), args[1].(time.Duration), args[2].(error)
// }

func Test_lookup(t *testing.T) {
	q := []*dns.Question{{
		Qclass: 1,
		Qtype:  dns.TypeA,
		Name:   "tesla.com",
	}}
	a := []*mockRR{{
		H: dns.RR_Header{
			Name:     "tesla.com",
			Rrtype:   dns.TypeA,
			Class:    1,
			Ttl:      3600,
			Rdlength: 4,
		},
		Data: "1.2.3.4",
	},
	}
	aMsg := newMockMsg(q, a, nil, nil)
	aMsgNameError := newMockMsgNameError(q)
	nameservers := []string{"nameserver1", "nameserver2"}
	mockLookupClient := new(mockLookupClient)
	mockLookupClient.On("Nameservers").Return(nameservers)
	// check that second nameserver is asked if first returns name error
	mockLookupClient.On("Exchange", mock.Anything, nameservers[0]).Return(aMsgNameError, time.Duration(1234), nil)
	mockLookupClient.On("Exchange", mock.Anything, nameservers[1]).Return(aMsg, time.Duration(1234), nil)
	ch := make(chan lookupReturn, 1)
	go lookup(mockLookupClient, "tesla.com", dns.TypeA, ch)
	expected := parseMsg(aMsg) // parseMsg ist tested seperately
	ret := <-ch
	equal := reflect.DeepEqual(expected, ret.res)
	if !equal {
		fmt.Printf("Missmatch\n\t\texpected: %+v\n\t\tactual:   %+v\n", expected, ret.res)
	}
	assert.Equal(t, true, equal)
}
