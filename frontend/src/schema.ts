export enum RecordTypes {
	A = 'A',
	AAAA = 'AAAA',
	ANY = 'ANY',
	CAA = 'CAA',
	CNAME = 'CNAME',
	DNSKEY = 'DNSKEY',
	DS = 'DS',
	MX = 'MX',
	NS = 'NS',
	PTR = 'PTR',
	SOA = 'SOA',
	SRV = 'SRV',
	TLSA = 'TLSA',
	TSIG = 'TSIG',
	TXT = 'TXT',
}

export type DNSLookupError = {
	message: string;
};

export interface DNSLookupParams {
	domain: string;
	types: string;
}

export interface DNSMultiLookupParams extends DNSLookupParams {
	nameservers: string;
}

export interface DNSLookupResult {
	Id: number;
	Response: boolean;
	Opcode: number;
	Authoritative: boolean;
	Truncated: boolean;
	RecursionDesired: boolean;
	RecursionAvailable: boolean;
	Zero: boolean;
	AuthenticatedData: boolean;
	CheckingDisabled: boolean;
	Rcode: number;
	Question: Question[];
	Answer: Answer[];
	Ns: Ns[];
	Extra: Extra[];
}

export interface Answer {
	Hdr: Hdr;
	Mx: string;
	Data: string;
}

export type Ns = Answer;

export type Extra = Answer;

export interface Hdr {
	Name: string;
	Rrtype: string;
	Class: number;
	Ttl: number;
	Rdlength: number;
}

export interface Question {
	Name: string;
	Qtype: number;
	Qclass: number;
}
