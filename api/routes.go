package main

import (
	"fmt"
	"strings"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/miekg/dns"
	"golang.org/x/net/idna"
)

func getLookup(dnsClientFactory lookupClientFactory) gin.HandlerFunc {
	return func(c *gin.Context) {
		var params lookupQueryParams
		err := c.BindQuery(&params)
		types := strings.Split(params.Types, ",")
		fmt.Print(types)

		if err != nil {
			sendBadRequest(c, "Invalid query params")
			return
		}

		domain, err := idna.ToASCII(dns.Fqdn(params.Domain))
		if err != nil {
			sendBadRequest(c, "Invalid domain")
			return
		}
		if _, isDomain := dns.IsDomainName((domain)); !isDomain {
			sendBadRequest(c, "Domain not well formatted")
			return
		}

		l := len(types)
		ch := make(chan lookupReturn, l)

		for _, paramType := range types {
			entryType, ok := dns.StringToType[strings.ToUpper((paramType))]
			if !ok {
				sendBadRequest(c, fmt.Sprintf("Unknown DNS record type %s", paramType))
				return
			}
			go func() {
				c := dnsClientFactory(nil)
				lookup(c, domain, entryType, ch)
			}()
		}

		responses := make([]*lookupResponse, l)
		for i := 0; i < l; i++ {
			if ret := <-ch; ret.err == nil {
				res := ret.res
				switch res.Rcode {
				case dns.RcodeSuccess:
					responses[i] = res
				case dns.RcodeNameError:
					sendNotFound(c, "Domain not found: "+domain)
					return
				default:
					sendInternal(c, fmt.Sprintf("Unhandled message rcode %d: %s", res.Rcode, dns.RcodeToString[int(res.Rcode)]), nil)
					return
				}
			} else {
				sendInternal(c, "DNS lookup failed", ret.err)
				return
			}
		}
		c.JSON(200, responses)
	}
}

func getLookupNameservers(dnsClientFactory lookupClientFactory) gin.HandlerFunc {
	return func(c *gin.Context) {
		var params lookupNameserversQueryParams
		err := c.BindQuery(&params)
		types := strings.Split(params.Types, ",")
		nameservers := strings.Split(params.Nameservers, ",")

		if err != nil {
			sendBadRequest(c, "Invalid query params")
			return
		}

		domain, err := idna.ToASCII(dns.Fqdn(params.Domain))
		if err != nil {
			sendBadRequest(c, "Invalid domain")
			return
		}
		if _, isDomain := dns.IsDomainName((domain)); !isDomain {
			sendBadRequest(c, "Domain not well formatted")
			return
		}

		l := len(types)
		lNs := len(nameservers)

		if l == 0 {
			sendBadRequest(c, "You need to pass types")
		}
		if lNs == 0 {
			sendBadRequest(c, "You need to pass nameservers to query")
		}

		chs := make([]chan lookupReturn, lNs)
		for i := 0; i < lNs; i++ {
			chs[i] = make(chan lookupReturn, l)
		}

		for i := 0; i < lNs; i++ {
			ch := chs[i]
			for _, paramType := range types {
				entryType, ok := dns.StringToType[strings.ToUpper((paramType))]
				if !ok {
					sendBadRequest(c, fmt.Sprintf("Unknown DNS record type %s", paramType))
					return
				}
				go func(entryType uint16, nameserver string, ch chan lookupReturn) {
					client := dnsClientFactory([]string{nameserver})
					lookup(client, domain, entryType, ch)
				}(entryType, nameservers[i], ch)
			}
		}

		responses := make([][]*lookupResponse, lNs)
		for ii := 0; ii < lNs; ii++ {
			responses[ii] = make([]*lookupResponse, l)
			for i := 0; i < l; i++ {
				if ret := <-chs[ii]; ret.err == nil {
					res := ret.res
					switch res.Rcode {
					case dns.RcodeSuccess:
						responses[ii][i] = res
						continue
					case dns.RcodeNameError:
						responses[ii][i] = res
						continue
					default:
						sendInternal(c, fmt.Sprintf("Unhandled message rcode %d: %s", res.Rcode, dns.RcodeToString[int(res.Rcode)]), nil)
						return
					}
				} else {
					sendInternal(c, "DNS lookup failed", ret.err)
					return
				}
			}
		}
		c.JSON(200, responses)
	}
}

func setupRouter(createLookupClient lookupClientFactory) *gin.Engine {
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
	}))
	r.GET("/lookup", getLookup(createLookupClient))
	r.GET("/lookupNameservers", getLookupNameservers(createLookupClient))
	r.GET("/", func(c *gin.Context) {
		c.String(200, "dig api")
	})
	return r
}
