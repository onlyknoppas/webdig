import React, { useState } from 'react';
import reactLogo from './assets/react.svg';
import './App.css';
import { useGet } from './query';
import {
	Alert,
	Button,
	Card,
	Col,
	Descriptions,
	Empty,
	Input,
	Row,
	Space,
	Spin,
	Switch,
	Tag,
	Typography,
} from 'antd';
import {
	Answer,
	DNSLookupError,
	DNSLookupParams,
	DNSLookupResult,
	DNSMultiLookupParams,
	RecordTypes,
} from './schema';
import formatDuration from 'date-fns/formatDuration';
import 'antd/dist/antd.css';

const NameserverEntry = ({ name, onDelete }: { name: string; onDelete: () => void }) => {
	return (
		<Tag>
			{name}
			<Button size="small" onClick={() => onDelete()}>
				-
			</Button>
		</Tag>
	);
};

const NameserversAddEntryButton = ({
	onAdd,
	children,
}: {
	onAdd: (name: string) => void;
	children: React.ReactNode;
}) => {
	const [showInput, setShowInput] = useState(false);
	const [value, setValue] = useState('');

	return (
		<Space>
			{showInput && <Input value={value} onChange={(e) => setValue(e.target.value)} />}
			<Button onClick={() => setShowInput(!showInput)}>
				{showInput ? 'Cancel' : children}
			</Button>
			{showInput && (
				<Button
					onClick={() => {
						setValue('');
						setShowInput(false);
						onAdd(value);
					}}
				>
					Add
				</Button>
			)}
		</Space>
	);
};

const NameserversEntry = ({
	onDelete,
	nameservers,
	onAdd,
}: {
	onDelete: (name: string) => void;
	onAdd: (name: string) => void;
	nameservers: string[];
}) => {
	return (
		<Space wrap>
			{nameservers.map((name) => (
				<NameserverEntry key={name} name={name} onDelete={() => onDelete(name)} />
			))}
			<NameserversAddEntryButton onAdd={onAdd}>
				{nameservers.length === 0 ? 'Add a custom nameserver' : 'Add'}
			</NameserversAddEntryButton>
		</Space>
	);
};

const InputForm = ({
	onQuery,
}: {
	onQuery: (params: DNSLookupParams | DNSMultiLookupParams) => void;
}) => {
	const [domain, setDomain] = useState<string>();
	const [type, setType] = useState<RecordTypes>();
	const [alertMessage, setAlertMessage] = useState<string>();
	const [nameservers, setNameservers] = useState<string[]>([]);

	const buttonOnClick = (k: string) => () => {
		if (domain == null || domain == '') {
			setAlertMessage('Please input a domain');
			return;
		}
		let param: string;
		if (k == RecordTypes.ANY) {
			param = Object.keys(RecordTypes)
				.filter((k) => k !== 'ANY')
				.join();
		} else param = k;
		setType(k as RecordTypes);
		if (nameservers.length > 0)
			onQuery({ domain, types: param, nameservers: nameservers.join() });
		else onQuery({ domain, types: param });
		setAlertMessage(undefined);
	};

	return (
		<Space direction="vertical" style={{ width: '100%' }}>
			{alertMessage && <Alert type="error" message={alertMessage} />}
			<Input onChange={(e) => setDomain(e.target.value)} value={domain} />
			<Space wrap>
				{Object.keys(RecordTypes).map((k) => (
					<Button
						key={k}
						type={type === k ? 'primary' : 'default'}
						onClick={buttonOnClick(k)}
					>
						{k}
					</Button>
				))}
			</Space>
			<NameserversEntry
				nameservers={nameservers}
				onAdd={(name) => setNameservers((ns) => [...ns, name])}
				onDelete={(name) => setNameservers((ns) => ns.filter((n) => n !== name))}
			></NameserversEntry>
			<Typography.Text type="secondary">
				do not forget the port number (usually 53)
			</Typography.Text>
		</Space>
	);
};

const RawResult = ({ data }: { data: DNSLookupResult[] }) => {
	console.log(data);
	return (
		<Typography.Paragraph style={{ textAlign: 'left' }} code>
			<pre>{JSON.stringify(data, null, 4)}</pre>
		</Typography.Paragraph>
	);
};

const ResultEntry = ({ data }: { data: Answer }) => {
	return (
		<Card title={data.Mx}>
			<Descriptions>
				<Descriptions.Item label="TYPE">{data.Hdr.Rrtype}</Descriptions.Item>
				<Descriptions.Item label="TTL">
					{formatDuration({ seconds: data.Hdr.Ttl })}
				</Descriptions.Item>
				<Descriptions.Item label="LENGTH">{data.Hdr.Rdlength}</Descriptions.Item>
				<Descriptions.Item label="DATA">{data.Data}</Descriptions.Item>
			</Descriptions>
		</Card>
	);
};

const LookupResult = ({ data }: { data: DNSLookupResult }) => {
	return (
		<>
			{data.Answer.map((a) => (
				<ResultEntry key={a.Mx} data={a} />
			))}
			{data.Ns.map((a) => (
				<ResultEntry key={a.Mx} data={a} />
			))}
			{data.Extra.map((a) => (
				<ResultEntry key={a.Mx} data={a} />
			))}
			;
		</>
	);
};

const LookupResults = ({ data }: { data: DNSLookupResult[] }) => {
	const [showRaw, setShowRaw] = useState(false);

	if (data.length === 0 || data.map((d) => d.Answer.length === 0).every((x) => x)) {
		return <Empty>No DNS records of this type</Empty>;
	}

	return (
		<Space align="start" size="large" direction="vertical" style={{ width: '100%' }}>
			{data.map((r) => (
				<LookupResult key={r.Id} data={r} />
			))}
			<Space size="middle">
				<Typography.Text>Show details</Typography.Text>
				<Switch checked={showRaw} onChange={(checked) => setShowRaw(checked)} />
			</Space>
			{showRaw && <RawResult data={data} />}
		</Space>
	);
};

const LookupMultiple = ({ params }: { params?: DNSMultiLookupParams }) => {
	const { data, isLoading, isError, error } = useGet<
		DNSLookupResult[][],
		DNSMultiLookupParams | undefined,
		DNSLookupError
	>('/lookupNameservers', params, { enabled: params != null });

	if (isLoading) return <Spin />;

	if (isError) return <Alert message={error.message} />;

	const nameserversArray = params?.nameservers.split(',');
	if (data != null)
		return (
			<Space direction="vertical">
				{data.map((d, i) => (
					<div key={i}>
						<Typography.Title level={3}>
							{nameserversArray && nameserversArray[i]}
						</Typography.Title>
						<LookupResults data={d} />
					</div>
				))}
			</Space>
		);

	return <Empty />;
};

const Lookup = ({ params }: { params?: DNSLookupParams }) => {
	const { data, isLoading, isError, error } = useGet<
		DNSLookupResult[],
		DNSLookupParams | undefined,
		DNSLookupError
	>('/lookup', params, { enabled: params != null });

	if (isLoading) return <Spin />;

	if (isError) return <Alert message={error.message} />;

	if (data != null) return <LookupResults data={data} />;

	return <Empty />;
};

const App = () => {
	const [params, setParams] = useState<DNSLookupParams | DNSMultiLookupParams>();

	return (
		<Row>
			<Col>
				<Space align="start" direction="vertical" size="large" style={{ width: '100%' }}>
					<Typography.Title code>dig</Typography.Title>
					<InputForm onQuery={setParams} />
					{params != null && 'nameservers' in params && params.nameservers.length > 0 ? (
						<LookupMultiple params={params} />
					) : (
						<Lookup params={params} />
					)}
				</Space>
			</Col>
		</Row>
	);
};

export default App;
