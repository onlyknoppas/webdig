package main

import "github.com/miekg/dns"

type lookupResponse struct {
	ID                 int64      `json:"Id"`
	Response           bool       `json:"Response"`
	Opcode             int64      `json:"Opcode"`
	Authoritative      bool       `json:"Authoritative"`
	Truncated          bool       `json:"Truncated"`
	RecursionDesired   bool       `json:"RecursionDesired"`
	RecursionAvailable bool       `json:"RecursionAvailable"`
	Zero               bool       `json:"Zero"`
	AuthenticatedData  bool       `json:"AuthenticatedData"`
	CheckingDisabled   bool       `json:"CheckingDisabled"`
	Rcode              int64      `json:"Rcode"`
	Question           []question `json:"Question"`
	Answer             []answer   `json:"Answer"`
	NS                 []nS       `json:"Ns"`
	Extra              []extra    `json:"Extra"`
}

type x dns.RR_Header

type answer struct {
	Hdr  hdr    `json:"Hdr"`
	MX   string `json:"Mx"`
	Data string `json:"Data"`
}

type extra answer

type nS answer

type hdr struct {
	Name     string `json:"Name"`
	Rrtype   string `json:"Rrtype"`
	Class    int64  `json:"Class"`
	Ttl      int64  `json:"Ttl"`
	Rdlength int64  `json:"Rdlength"`
}

type question struct {
	Name   string `json:"Name"`
	Qtype  string `json:"Qtype"`
	Qclass int64  `json:"Qclass"`
}

type lookupQueryParams struct {
	Domain string `form:"domain"`
	Types  string `form:"types"`
}

type lookupNameserversQueryParams struct {
	Domain      string `form:"domain"`
	Types       string `form:"types"`
	Nameservers string `form:"nameservers"`
}

type lookupReturn struct {
	res *lookupResponse
	err error
}
