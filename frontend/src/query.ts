import axios, { AxiosRequestHeaders } from 'axios';
import { QueryFunction, QueryKey, useQuery, UseQueryOptions } from 'react-query';

export const useGet = <ResponseType, ParamsType, ErrorType>(
	endpoint: string,
	params: ParamsType,
	options: Omit<
		UseQueryOptions<unknown, ErrorType, ResponseType, (string | ParamsType)[]>,
		'queryKey' | 'queryFn'
	> = {},
) => {
	const query: QueryFunction<ResponseType, QueryKey> = async ({ queryKey }) => {
		const params = queryKey[1];

		try {
			return (
				await axios.get<ResponseType>(import.meta.env.VITE_API_URL + endpoint, {
					params,
				})
			).data;
		} catch (error) {
			console.error(error);
			throw error;
		}
	};

	return useQuery([endpoint, params], query, options);
};
