package main

import (
	"errors"
	"time"

	"github.com/miekg/dns"
)

type dnsExchanger interface {
	Exchange(m *dns.Msg, ns string) (*dns.Msg, time.Duration, error)
	Nameservers() []string
}

var defaultNameservers = [...]string{
	"1.1.1.1:53",
	"8.8.8.8:53",
}

type lookupClient struct {
	dns.Client
	nameservers []string
}

func (l *lookupClient) Nameservers() []string {
	return l.nameservers
}

func createLookupClient(nameservers []string) dnsExchanger {
	c := new(lookupClient)
	if len(nameservers) == 0 {
		c.nameservers = defaultNameservers[:]
	} else {
		c.nameservers = nameservers
	}
	return c
}

type lookupClientFactory func(nameservers []string) dnsExchanger

func lookup(c dnsExchanger, domain string, entryType uint16, ch chan lookupReturn) {
	m := new(dns.Msg)
	m.SetQuestion(domain, entryType)
	m.MsgHdr.RecursionDesired = true

	for i, ns := range c.Nameservers() {
		if in, _, err := c.Exchange(m, ns); err == nil {
			if in.Rcode == dns.RcodeNameError && i < len(c.Nameservers())-1 {
				continue
			}
			ch <- lookupReturn{parseMsg(in), nil}
			return
		} else {
			ch <- lookupReturn{nil, err}
			return
		}
	}
	ch <- lookupReturn{nil, errors.New("lookup exited unexpectedly")}
}
